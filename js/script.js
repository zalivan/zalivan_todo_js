$(document).ready( function () {
    let todoList = [];
    let displayShow = "all";
    let tempShowList = [];
    const PaginationLength = 5;
    let paginationList = 0;
    let todoPaginationArr = [];
    let tempButton = false;
    let $selectorInput = $("input");
    let $selectorBtn = $("button");
    let dataTodoId = "data-todoid";
    let $todoInput = $("#todo__input");
    let $todoUlList = $("#todo__list");
    let $divError = "<div class='alert alert-danger' id ='error'>Enter correct todo!!!</div>";

    $("#todo__enter").on("click", add);
    $("#todo__re-choose-all").on("click", todoUncheckAll);
    $("#todo__choose-all").on("click", todoCheckAll);
    $("#todo__delete-all").on("click", todoDeleteAll);
    $("#todo__delete-ended").on("click", todoDeleteCompleted);
    $("#display-all").on("click", displayAll);

    $("#display-active").on("click", displayActive);

    $("#display-completed").on("click", displayCompleted);

    $todoInput.keydown(function (e) {
        e = e || window.event;
        if (e.keyCode === 13) {
            add();
        }
    });

    function add() {
        let todoInput = $todoInput;
        $("#error").remove();

        if (todoInput.val().trim() === "") {
            todoInput.after($divError);
            return false;
        }

        let tempTodo = {};
        tempTodo.todoId = Number(new Date());
        tempTodo.name = todoInput.val().trim();
        tempTodo.check = false;

        todoList.push(tempTodo);
        tempButton = false;

        ShowAll();
    }

    function ShowAll() {
        todoCounter();
        todoDisplayListCompleted();
        todoPagination();

        let todoUlHtml = "";
        let todoLiSelector = "";
        $("[data-id__nav='" + (paginationList + 1) + "']").addClass('active');

        _.forEach( todoPaginationArr[paginationList] , function (obj) {
            let tempCheck;
            if (obj.check === true) {
                tempCheck = "checked";
            } else {
                tempCheck = "";
            }

            todoLiSelector = "<li class='list__item  col-lg-12'" + dataTodoId + "='" + obj.todoId + "'>" +
            "<div class='input-group col-lg-12 item__block'" + dataTodoId + "='" + obj.todoId + "'>" +
            "<input type='checkbox' class='item__check'" + dataTodoId + "='" + obj.todoId + "' " + tempCheck + ">" +
            "<div class='task-title  item__name'>" +
            "<input class='task-title-sp item__name' disabled value='" + obj.name + "'" + dataTodoId + "='" + obj.todoId + "'>" +
            "</div>" +
            "<button class='btn btn-primary btn-sm item__edit'" + dataTodoId + "='" + obj.todoId + "'>" +
            "<i class='glyphicon glyphicon-pencil'" + dataTodoId + "='" + obj.todoId + "'></i>" +
            "</button>" +
            "<button class='btn btn-danger btn-sm item__remove'" + dataTodoId + "='" + obj.todoId + "'>" +
            "<i class='glyphicon glyphicon-remove'" + dataTodoId + "='" + obj.todoId + "'></i>" +
            "</button>" +
            "</div></li>";

            todoUlHtml += todoLiSelector;
            $todoInput.val("");
        });

        $todoUlList.html( todoUlHtml );

        $(".item__remove").on("click", todoDeleteOne);
        $(".item__check").on("change", todoEnded);
        $(".item__edit").on("click", todoEdit);
        $(".item__name").on("dblclick", todoEdit);
        $(".pagination__nav").on("click", function (e) {
            paginationList = parseInt($(e.currentTarget).attr("data-id__nav")) - 1;
            $todoUlList.val("");
            tempButton = true;
            ShowAll();
        });
    }

    function todoDeleteOne(event) {
        let taskId = parseInt($(event.currentTarget).attr(dataTodoId));
        let indexRemove = null;
        _.each(todoList, function (obj, index) {
            if (obj.todoId === taskId) {
                indexRemove = index;
            }
        });

        todoList.splice(indexRemove, 1);

        if ( !todoPaginationArr[todoPaginationArr.length - 1] ) {
            tempButton = true;
        }

        displayAll();
        ShowAll();
    }

    function todoEnded(e) {
        let $currentTargetSelector = $(e.currentTarget);
        let tempId = parseInt($currentTargetSelector.attr(dataTodoId));
        if ($currentTargetSelector.prop("checked")) {
            _.forEach(todoList, function (obj) {
                if (obj.todoId === tempId) {
                    obj.check = !obj.check;
                }
            });
        } else {
            _.forEach(todoList, function (obj) {
                if (obj.todoId === tempId) {
                    obj.check = !obj.check;
                }
            });
        }

        ShowAll();
    }

    function todoDeleteAll() {
        todoList.splice(0, todoList.length);

        ShowAll();
    }

    function todoDeleteCompleted() {
        let tempDeleteList = [];

        _.each(todoList, function (obj) {
            if (obj.check === false) {
                tempDeleteList.push(obj);
            }
        });

        todoList = tempDeleteList;

        displayAll();
        ShowAll();
    }

    function todoCheckAll() {
        _.forEach(todoList, function (obj) {
            obj.check = true;
        });

        ShowAll();
    }

    function todoUncheckAll() {
        _.forEach(todoList, function (obj) {
            obj.check = false;
        });

        ShowAll();
    }

    function todoEdit(e) {
        $selectorInput.prop("disabled", true);
        $selectorBtn.prop("disabled", true);
        let $selectorItemBlock = $(".item__block");
        $selectorItemBlock.css({'opacity': '0'});
        $(".pagination__nav").css({'opacity': '0'});
        let tempEditDiv = $(e.currentTarget).parent( $selectorItemBlock );
        let tempEditId = parseInt(tempEditDiv.attr(dataTodoId));

        let tempEditObject = _.find( todoList, function (obj) {
            if (obj.todoId === tempEditId) {
                return this;
            }
        });

        tempEditDiv.addClass("hide__element");

        let todoTempLiSelector = "<div>" +
            "<input class='item__edit-name' value='" + tempEditObject.name + "'>" +
            "<button class='glyphicon glyphicon-ok-sign btn btn-success item__edit-add'></button>" +
            "<button class='glyphicon glyphicon-remove-sign btn btn-danger item__edit-cancel'></button></div>";

        tempEditDiv.parents(".list__item").html( todoTempLiSelector );

        let tempEditArray = [],
        todoItemEditName = $(".item__edit-name");

        todoItemEditName.focus();

        todoItemEditName.keydown(function (e) {
            e = e || window.event;
            if (e.keyCode === 13) {
                tempEditAdd();
            } else if (e.keyCode === 27) {
                tempEditCancel();
            }
        });

        $(".item__edit-add").on("click", tempEditAdd);

        function tempEditAdd() {
            $selectorInput.prop("disabled", true);
            $selectorBtn.prop("disabled", true);

            tempEditArray = _.forEach(todoList, function (obj) {
                if (obj.todoId === tempEditObject.todoId) {
                    if ( todoItemEditName.val().trim().length === 0 || todoItemEditName.val().trim() === "" ) {
                        $selectorInput.prop("disabled", true);
                        $selectorBtn.prop("disabled", true);
                        todoItemEditName.after($divError);

                        obj.name = tempEditObject.name;
                        tempEditAdd();
                    } else {
                        obj.name = todoItemEditName.val().trim();
                    }
                }
                $selectorInput.prop("disabled", false);
                $selectorBtn.prop("disabled", false);
            });

            todoList = tempEditArray;

            ShowAll();
        }

        $(".item__edit-cancel").on("click", tempEditCancel);

        function tempEditCancel() {
            tempEditArray = _.forEach(todoList, function (obj) {
                if (obj.todoId === tempEditObject.todoId) {
                    obj.name = tempEditObject.name;
                }
            });
            todoList = tempEditArray;

            $selectorInput.prop("disabled", false);
            $selectorBtn.prop("disabled", false);

            ShowAll();
        }
    }

    function todoCounter() {
        let CountEnded = 0;
        let CountActive = 0;

        _.forEach(todoList, function (obj) {
            if (obj.check) {
                CountEnded++;
            } else {
                CountActive++;
            }
        });

        todoList.countEnded = CountEnded;
        todoList.countActive = CountActive;
        todoList.countAll = todoList.countActive + todoList.countEnded;

        if (CountActive || CountEnded) {
            $(".count").removeClass("hide");
            $(".pagination").removeClass("hide");
        }

        $("#count__active").text(todoList.countActive);
        $("#count__ended").text(todoList.countEnded);
        $("#count__all").text(todoList.countAll);
    }

    function todoDisplayListCompleted() {
        let $displayListSelector = $(".display-list");
        let $todoCenterBtnSelector = $(".todo__center-buttons");
        if (!todoList.name) {
            $displayListSelector.removeClass("hide");
            $todoCenterBtnSelector.removeClass("hide");
        } else {
            $displayListSelector.addClass("hide");
            $todoCenterBtnSelector.addClass("hide");
        }

        $todoUlList.children(".list__item").remove();

        tempShowList = [];

        if (displayShow === "all") {
            tempShowList = todoList;
        } else if (displayShow === "active") {
            _.forEach(todoList, function (obj) {
                if (obj.check === false) {
                    tempShowList.push(obj);
                }
            });
        } else if (displayShow === "completed") {
            _.forEach(todoList, function (obj) {
                if (obj.check === true) {
                    tempShowList.push(obj);
                }
            });
        }
    }

    function todoPagination() {
        todoPaginationArr = _.chunk( tempShowList, PaginationLength );
        let pagination = $("#pagination");
        pagination.html( "" );
        let paginationHTML = "";
        let paginationLi = "";

        for ( let i = 1; i <= todoPaginationArr.length; i++ ) {
            paginationLi = "<li class='pagination__nav' data-id__nav='" + i + "'><span>" + i +
                "<span class='sr-only'></span></span></li>";
            paginationHTML += paginationLi;
        }

        pagination.html( paginationHTML );

        if ( !tempButton ) {
            paginationList = todoPaginationArr.length - 1;
        }

        if ( paginationList >= todoPaginationArr.length ) {
            paginationList--;
        }
    }

    function displayAll() {
        displayShow = "all";
        ShowAll();
    }

    function displayActive() {
        displayShow = "active";
        paginationList = 1;
        ShowAll();
    }

    function displayCompleted() {
        displayShow = "completed";
        paginationList = 1;
        ShowAll();
    }
});